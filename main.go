package main

import (
	"fmt"
	"log"
	"net/http"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func handler(w http.ResponseWriter, r *http.Request) {

	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		http.Error(w, "could not get incluster config", 500)
		return
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		http.Error(w, "could not setup connection with retrieved incluster config", 500)
		return
	}

	pods, err := clientset.CoreV1().Pods("").List(metav1.ListOptions{})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	fmt.Fprintf(w, "There are %d pods in the cluster\n", len(pods.Items))
}

func main() {

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":80", nil))
}
